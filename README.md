## elePHPant.CMF
Base for the construction of a complete content management system.

### Installation
```sh
$ git clone git@bitbucket.org:elephpantpl/elephpant.cmf.git elePHPant
$ cd elePHPant
$ vim app\config\development.php | change your information in it
$ mysql elephpant < database.sql
```

### License
Read file: [LICENSE].

[LICENSE]:/LICENSE.md